let fruits = ["Apple", "Orange", "Kiwi", "Watermelon"];

/*['Apple', 'Orange', 'Kiwi', 'Watermelon']*/

/*Push() Method*/
console.log("Current Fruits Array:");
console.log(fruits);

fruits.push("Mango");
console.log(fruits);

let fruitsLength = fruits.push("Melon");
console.log(fruitsLength);

fruits.push("Avocado", "Guava")
console.log(fruits)


/*['Apple', 'Orange', 'Kiwi', 'Watermelon', 'Mango']*/




/*pop() Method*/
let removedFruit = fruits.pop()
console.log(removedFruit)
console.log("Mutated array from the pop method:")
console.log(fruits)

/*unshift() Method*/
fruits.unshift("Lime", "Banana")
console.log("Mutated array from the unshift method:")
console.log(fruits)


/*shift() Method*/
let removedFruit2 = fruits.shift()
console.log(removedFruit2)
console.log("Mutated array from the shift method:")
console.log(fruits)


/*splice() Method*/
let fruitsSplice = fruits.splice(1, 2, "Cherry", "Lychee")
console.log("Mutated arrays from splice method: ")
console.log(fruits)
console.log(fruitsSplice)

fruits.sort()
console.log("Mutated array from sort Method:")
console.log(fruits)

let mixedArr = [12, "May", 36, 94, "August", 5, 6.3, "September"]
console.log(mixedArr.sort())



/*reverse() method*/
fruits.reverse()
console.log("Mutated array from reverse method: ")
console.log(fruits)


let mixedArr2 = [12, "May", 36, 94, "August", 5, 6.3, "September"]
console.log(mixedArr.reverse())


/*Mini Activity

 - Debug the function which will allow us to list fruits in the fruits array.
	 	-- this function should be able to receive a string.
	 	-- determine if the input fruit name already exist in the fruits array.
	 		*** If it does, show an alert message: "Fruit already listed on our inventory".
	 		*** If not, add the new fruit into the fruits array ans show an alert message: "Fruit is not listed in our inventory."
	 	-- invoke and register a new fruit in the fruit array.
	 	-- log the updated fruits array in the console
	 	*/




	 	function registerFruit () {
	 		let fruitName = "Tomato";
	 		let doesFruitExist = fruits.includes(fruitName);


	 		if(doesFruitExist) {
	 			alert(fruitName + " is already on our inventory");
	 		} else {
	 			fruits.push(fruitName);
	 		
	 			alert("Fruit is now listed in our inventory");
	 			console.log(fruits);
	 				
	 		}

	 	}
registerFruit();

/*Mini Activity 2*/

let users = ["Joe", "Tim", "Doe", "John"];

/*Push() Method*/
console.log("Users (push method): ");
console.log(users);

users.push("jango");
console.log(users);

/*unshift() Method*/
users.unshift("corah", "jim")
console.log("Users (unshift method): ")
console.log(users)

/*pop() Method*/
let removedUsers = users.pop()
console.log(removedUsers)
console.log("Users (pop method):")
console.log(users)

/*shift() Method*/
let removedUsers2 = users.shift()
console.log(removedUsers2)
console.log("Users (shift method):")
console.log(users)

users.sort()
console.log("Users (sort method): ")
console.log(users)

/*
	Non-Mutator Methods
		- These are functions or methods that don not modify or change an array after they are created.
		- These methods do not maniplutae the original array performing various tasks such as returning elements from an array and combining arrays and printing the output.
	
*/

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"]
console.log(countries)

/*
	indexOf()

		- returns the index number of the first matching element found in an array. If no match as found, the result will be -1. The search process will be done from our first element proceeding to the last element

	Syntax:
		arrayName.indexOf(searchValue)
		arrayName.indexOPf(searchValue, fromIndex)

*/


let firstIndex = countries.indexOf("PH")
console.log("Result of indexOf method: " + firstIndex)
firstIndex = countries.indexOf("PH", 4)
console.log("Result of indexOf method: " + firstIndex)
firstIndex = countries.indexOf("PH", 7)
console.log("Result of indexOf method: " + firstIndex)
firstIndex = countries.indexOf("PH", -1)
console.log("Result of indexOf method: " + firstIndex)

console.log(" ")


/*
	lastIndexOf
		- returns the index number of the last matching element found in an array. The search process will be done from the last element proceeding to the first.
	Syntax: 
		arrayName.lastIndexOf(searcValue)
		arrayName.lastIndexOf(searcValue, fromIndex)

*/

let lastIndex = countries.lastIndexOf("PH")
console.log("Result of lastIndexOf method: " + lastIndex)
lastIndex = countries.lastIndexOf("PH", 4)
console.log("Result of lastIndexOf method: " + lastIndex)

/*
	slice()
		- portions / slices elements from our array and returns a new array

	Syntax:
		arrayName.slice(startingIndex)
		arrayName.slice(startingIndex, endingIndex)

*/

console.log(countries)

let slicedArrA = countries.slice(2)
console.log("Result from slice method:")
console.log(slicedArrA)
console.log(countries)

console.log(" ")

let slicedArrB = countries.slice(2, 5)
console.log("Result from slice method:")
console.log(slicedArrB)
console.log(countries)

let slicedArrC = countries.slice(-3)
console.log("Result from slice method:")
console.log(slicedArrC)
console.log(countries)

/*
	toString()
		- returns an array as a string separated by commas
		- is used internally by JS when an array needs to be displayed as text(like in HTML), or when an object/array needs to be used as a string.

	Syntax:
		arrayName.toString()
*/


let stringArray = countries.toString()
console.log("Result from toString method:")
console.log(stringArray)

let mixedArrToString = mixedArr.toString()
console.log(mixedArrToString)

/*
	concat()
		- combines two or more arrays and returns the combined result

	Syntax:
		arrayA.concat(arrayB)
		arrayA.concat(elementA)

*/

let taskArrayA = ["drink HTML", "eat Javascript"]
let taskArrayB = ["inhale css", "breath sass"]
let taskArrayC = ["get git", "be node"]

let tasks = taskArrayA.concat(taskArrayB)
console.log("Result from concat method:")
console.log(tasks)

let allTasks = taskArrayA.concat(taskArrayB, taskArrayC)
console.log(allTasks)

// Combining arrays with element - similar with push()
let combineTask = taskArrayA.concat("smell express", "throw react")
console.log(combineTask)

/*
	join()
		- returns an array as a string
		- does not change the original array
		- we can use any separator. The default is comma (,)

	Syntax:
		arrayName.join(separatorString)
*/

let students = ["Elysha", "Gab", "Ronel", "Jean"]
console.log(students.join())
console.log(students.join(' '))
console.log(students.join(" - "))

/*
	Iteration Methods
		- are loops designed to perform repitive tasks in an array. sued for manipulating array data resulting in complex tasks.
		- noramlly work with a function supplied as an argument
		- aims to evaluate each element in an array
*/

/*
	forEach()
		- similar to for loop that iterates on each array element

	Syntax:
		arrayName.forEach(function(individualElement) {
			statement
		})

*/

allTasks.forEach(function(task) {
	console.log(task)
})

console.log(" ")
// Using forEach with conditional statements

let filteredTasks = []

allTasks.forEach(function(task) {
	console.log(task)
	if(task.length > 10) {
		filteredTasks.push(task)
	}
})

console.log(allTasks)
console.log("Result of filteredTasks: ")
console.log(filteredTasks)

/*
	map()
		- iterates on each element and returns a new array with different value depending on the result of the function's operation

	Syntax:
		arrayName.map(function(individualElement) {
			statement
		})
*/

let numbers = [1, 2, 3, 4, 5]

let numberMap = numbers.map(function(number) {
		console.log(number)
		return number*number
})

console.log("Original Array")
console.log(numbers)
console.log("Result of the map method:")
console.log(numberMap)


/*
	every()
		- checks if all elements in an array met the given condition. Returns a true value if all elements meet the condition and false if otherwise

	Syntax:
	 	arrayName.every(function(individualElement) {
			return expression/condition
	 	})

*/

let allValid = numbers.every(function(number) {
	return (number < 3);
})

console.log("Result of every method:")
console.log(allValid)


/*
	some()
	 	- checks if at least one element in the array meet the given condition. Returns a true value if at least one of the elements meets the given condition and false if otherwise.

	 Syntax:
	 	arrayName.some(function(ind){
			return expression/condition
	 	})

*/

let someValid = numbers.some(function(number) {
	return (number < 3) 
})

console.log("Result from some method:")
console.log(someValid)


/*
	filter()
		- returns a new array that contains elements wcich meets the given condition. Return an empty array in no elements were found (that satisfied the given condition)

	Syntax:
		arrayName.filter(function(individualElement){
			return expression/ condition
		})

*/

let filteredValid = numbers.filter(function(number) {
	return (number < 5)
})

console.log("Result of filter method:")
console.log(filteredValid)


/*
	includes()
		- checks if the argument passed can be found in an array
	- can be chained after another method. The result of the first method is used on the second method until all chained methods have been resolved.	

*/


let products = ["mouse", "KEYBOARD", "laptop", "monitor"]

let filteredProducts = products.filter(function(product){
	return product.toLowerCase().includes("a")
})


console.log("Result on includes method:")
console.log(filteredProducts)
